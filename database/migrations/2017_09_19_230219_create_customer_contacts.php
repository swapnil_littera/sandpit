<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerContacts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('Customer Contacts', function (Blueprint $table) {
			$table->increments('Contact Id');
			$table->integer('Customer Id'); 			
			$table->string('Customer Code', 10);
			$table->string('Contact Name');
           		$table->string('Contact Phone');
			$table->string('Contact Email', 100);
			$table->string('Role');
      	 	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Customer Contacts');

    }
}
