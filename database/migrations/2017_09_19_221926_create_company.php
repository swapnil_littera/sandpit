<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompany extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Company', function (Blueprint $table) {
		$table->increments('Company Id');  
		$table->integer('User Id');
		$table->string('Company Code'); 
		$table->string('Company Name'); 
		$table->date('Create_Date');       
		$table->string('Create_User', 100);
		$table->date('Update_Date');       
		$table->string('Update_User', 100);
       });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Company');

    }
}
