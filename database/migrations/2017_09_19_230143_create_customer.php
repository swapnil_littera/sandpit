<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('Customer', function (Blueprint $table) {
			$table->increments('Customer Id', true); 			
			$table->integer('Company Id');  
			$table->string('Customer Code', 10);
           		$table->string('Customer Name');
			$table->string('Customer Industry', 100);
      	 	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Customer');

    }
}
